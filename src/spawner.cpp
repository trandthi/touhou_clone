
#include "raylib.h"
#include "raymath.h"

#include "../include/spawner.hpp"
#include "../include/bullet.hpp"

using namespace std;
// HACK maybe create a seperate file for derived classes
// ------------------------------------------------------------ class SpawnerVec
SpawnerVec::SpawnerVec() {}
SpawnerVec::~SpawnerVec() {
    for (Spawner *spawner : spawners) {
        delete spawner;
    }
}

void SpawnerVec::add(Spawner *spawner) {
    spawners.push_back(spawner);
}

void SpawnerVec::update(BulletVec &bullets, const float &delta) {
    for ( auto sIt = spawners.begin(); sIt != spawners.end(); ) {
        (*sIt)->update(bullets, delta);
        if ( (*sIt)->ended() ) {
            delete(*sIt);
            spawners.erase(sIt);
        } else { sIt++; }
    }
}

void SpawnerVec::draw() {
    for ( Spawner *spawner : spawners ) {
        spawner->draw();
    }
}

// ------------------------------------------------------------ parent class Spawner
Spawner::Spawner(Vector2 pos) {
    this->pos = pos;
    reload = 0;
    angle = 0;
    end = false;
}

Spawner::~Spawner() {}

void Spawner::spawn(BulletVec &bullets, float rads) {}

void Spawner::update(BulletVec &bullets, const float &delta) {}

void Spawner::draw() {
    DrawCircle(pos.x, pos.y, 10, colour);
}
bool Spawner::ended() {
    if ( end == true ) return true;
    return false;
}

// ------------------------------------------------------------ derived class Annihilator
Annihilator::Annihilator(Vector2 pos) : Spawner(pos) {
    rtime = 0.1;
    colour = RED;
}
void Annihilator::spawn(BulletVec &bullets, float rads) { 
    bullets.add(new Bullet(pos, Vector2Rotate(Vector2{0,1},rads)));
    bullets.add(new Bullet(pos, Vector2Rotate(Vector2{0,1},rads + (1.0/2.0)*3.14)));
    bullets.add(new Bullet(pos, Vector2Rotate(Vector2{0,1},rads + 3.14)));
    bullets.add(new Bullet(pos, Vector2Rotate(Vector2{0,1},rads + (3.0/2.0)*3.14)));
}
void Annihilator::update(BulletVec &bullets, const float &delta) {
    if ( reload > rtime ) {
        spawn(bullets, angle*(3.14/180.0f));
        angle += 6;
        reload = 0;
        if (angle >= 360) {
            angle = 0;
            end = true;
        }
    }
    reload += delta;
}