#include <iostream>

#include "raylib.h"

#include "../include/player.hpp"
#include "../include/spawner.hpp"
#include "../include/bullet.hpp"

using namespace std;

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 800

int main () {

    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "touhouclone");
    SetTargetFPS(60);

    Player player(Vector2{400,400});
    
    BulletVec bulletVec;
    
    SpawnerVec spawnerVec;
    spawnerVec.add(new Annihilator(Vector2{266,200}));
    spawnerVec.add(new Annihilator(Vector2{532,200}));

    while (WindowShouldClose() == false){

        float delta = GetFrameTime();
// player gaming
        player.dir = Vector2{0,0};
        if (IsKeyDown(KEY_W)) player.dir.y = -1;
        if (IsKeyDown(KEY_A)) player.dir.x = -1;
        if (IsKeyDown(KEY_S)) player.dir.y = 1;
        if (IsKeyDown(KEY_D)) player.dir.x = 1;
        player.move(delta);

// update
        bulletVec.update(delta);
        spawnerVec.update(bulletVec, delta);

// collision check
        if (bulletVec.collisionCheck(player)) {
                player.health -= 1;
        }

// draw
        BeginDrawing();
        ClearBackground(BLACK);
        
        player.draw();
        spawnerVec.draw();
        bulletVec.draw();

        EndDrawing();
    }

    CloseWindow();
    return 0;
}