#include <iostream>
#include <cmath>

#include "raylib.h"
#include "raymath.h"

#include "../include/player.hpp"

using namespace std;

Player::Player(Vector2 xy) {
    pos = xy;
    speed = 250;
    dir = Vector2{0,0};
    radius = 10;
}

Player::~Player() {}

void Player::move(const float &delta) {
    dir = Vector2Normalize(dir);
    pos = Vector2Add(pos, Vector2Scale(dir,speed*delta));
}

void Player::draw() {
    DrawCircle(pos.x, pos.y, 10, GREEN);
}