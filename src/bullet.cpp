#include <cmath>
#include <iostream>

#include "raylib.h"
#include "raymath.h"

#include "../include/bullet.hpp"

// class BulletVec
BulletVec::BulletVec() {}

BulletVec::~BulletVec() {
    for (Bullet *bullet : bullets) {
        delete bullet;
    }
}

void BulletVec::add(Bullet *bullet) {
    bullets.push_back(bullet);
}

bool BulletVec::collisionCheck(const Player &player) {
    for ( Bullet *bullet : bullets) {
        if (CheckCollisionCircles(  player.pos, player.radius,
                                    bullet->pos, bullet->radius)) 
        { 
            return true;
        }
    }
    return false;
}

void BulletVec::update(const float &delta) {
    for ( auto bIt = bullets.begin(); bIt != bullets.end(); ) {
        if ( !((*bIt)->update(delta)) ) {
            delete(*bIt);
            bullets.erase(bIt);
        } else { bIt++; }
    }
}

void BulletVec::draw() {
    for ( Bullet *bullet : bullets ) {
        bullet->draw();
    }
}

// class Bullet
Bullet::Bullet(Vector2 pos, Vector2 dir) {
    this->pos = pos;
    this->dir = dir;
    speed = 200;
    ttl = 2;
    radius = 10;
}

Bullet::~Bullet() {}

void Bullet::move(const float &delta) {
    dir = Vector2Normalize(dir);
    pos = Vector2Add(pos, Vector2Scale(dir, speed*delta));
}

void Bullet::draw() {
    DrawCircle(pos.x, pos.y, 10, ORANGE);
}

bool Bullet::update(const float &delta) {
    ttl -= delta;
    if ( ttl < 0 ) { return false;}
    move(delta);
    return true;
}

