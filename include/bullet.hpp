#ifndef BULLET_HPP
#define BULLET_HPP

#include <vector>

#include "raylib.h"
#include "player.hpp"

using namespace std;

class Bullet {
    public:
        Vector2 pos;
        Vector2 dir;
        float speed;
        float ttl;
        float radius;

        Bullet(Vector2 pos, Vector2 dir);
        ~Bullet();
 
        void move(const float &delta);
        bool update(const float &delta);
        void draw();

};

class BulletVec {
    public:
        vector<Bullet*> bullets;

        BulletVec();
        ~BulletVec();
        void add(Bullet *bullet);
        bool collisionCheck( const Player &player);
        void update(const float &delta);
        void draw();
};


#endif