#ifndef SPAWNER_HPP
#define SPAWNER_HPP

#include <vector>

#include "raylib.h"

#include "../include/bullet.hpp"

using namespace std;

class Spawner {
    public:
        Vector2 pos;
        float reload;
        float rtime;
        float angle;
        Color colour;
        bool end;
 
        Spawner(Vector2 pos);
        virtual ~Spawner();
 
        virtual void update( BulletVec &bullets, const float &delta);
        virtual void spawn( BulletVec &bullets, float rads);
        virtual bool ended();
        void draw();
        
};

class SpawnerVec {
    public:
        vector<Spawner*> spawners;

        SpawnerVec();
        ~SpawnerVec();
        void add(Spawner *spawner);
        void update(BulletVec &bullets, const float &delta);
        void draw();
};
// annihilator, devastator, obliterator, exterminator, ruinator, desolator, eradicator
class Annihilator : public Spawner {
    public:

        Annihilator(Vector2 pos);

        void update( BulletVec &bullets, const float &delta);
        void spawn( BulletVec &bullets, float rads);
};

#endif