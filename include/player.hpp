#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "raylib.h"

using namespace std;

class Player {
    public:
        Vector2 pos;
        float speed;
        Vector2 dir;
        float radius;
        int health;
        bool invicible;

        void move(const float &delta);
        void draw();

        Player(Vector2 xy);
        ~Player();
};

#endif